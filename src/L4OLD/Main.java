package L4OLD;

import L4OLD.domain.Account;
import L4OLD.exception.NotEnoughMoneyException;
import L4OLD.exception.UnknownAccountException;
import L4OLD.repositry.FileRepository;


import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException, UnknownAccountException, NotEnoughMoneyException {
        Path path = Paths.get("C:\\Users\\MrDokter\\Downloads\\project\\untitled1\\src\\L4\\Acc");
        if (!Files.exists(path)) {
            new File("C:\\Users\\MrDokter\\Downloads\\project\\untitled1\\src\\L4\\Acc").mkdirs();
                Account acc0 = new Account(0, "Koma", 40000);
                Account acc1 = new Account(1111, "Oleg", 35000);
                Account acc2 = new Account(2222, "Korg", 38000);
                Account acc3 = new Account(3333, "Socr", 85000);
                Account acc4 = new Account(4444, "Vlad", 12000);
                Account acc5 = new Account(5555, "Anna", 73000);
                Account acc6 = new Account(6666, "Lena", 42000);
                Account acc7 = new Account(7777, "Max", 21000);
                Account acc8 = new Account(8888, "Tal", 94000);
                Account acc9 = new Account(9999, "Jesus", 27000);

                FileRepository.write(acc0);
                FileRepository.write(acc1);
                FileRepository.write(acc2);
                FileRepository.write(acc3);
                FileRepository.write(acc4);
                FileRepository.write(acc5);
                FileRepository.write(acc6);
                FileRepository.write(acc7);
                FileRepository.write(acc8);
                FileRepository.write(acc9);

        }

        Menu menu = new Menu();
        for (; ; ) {
            menu.showmenu();
            Scanner scanner = new Scanner(System.in);
            String choise = scanner.nextLine();
            System.out.println("\n");
            menu.starter(choise);
            if (Menu.quit){
                break;
            }
        }
    }
}
