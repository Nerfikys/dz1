package L4OLD;

import L4OLD.exception.NotEnoughMoneyException;
import L4OLD.exception.UnknownAccountException;
import L4OLD.repositry.FileRepository;
import L4OLD.service.FileAccountService;

public class Menu {
    public static boolean quit;

    void starter(String what) throws UnknownAccountException, NotEnoughMoneyException {
        String[] action = what.split(" ");
        switch (action[0]) {
            case "balance":
                System.out.println(FileAccountService.balance(Integer.parseInt(action[1])));
                break;
            case "withdraw":

                FileAccountService.withdraw(Integer.parseInt(action[1]),Double.parseDouble(action[2]));
                break;
            case "deposite [id] [amount]":
                System.out.println("deposite [id] [amount]");
            case "transfer [from] [to] [amount]":
                System.out.println("transfer [from] [to] [amount]");
                break;
            case "q":
                quit = true;
                break;
        }
        System.out.println();
    }

    void showmenu(){
        System.out.println("Меню:");
        System.out.println("    - при вводе в консоле команды balance [id] – вывеси информацию о счёте");
        System.out.println("    - при вводе в консоле команды withdraw [id] [amount] – снять указанную сумму");
        System.out.println("    - при вводе в консоле команды deposite [id] [amount] – внести на счет указанную сумму");
        System.out.println("    - при вводе в консоле команды transfer [from] [to] [amount] – перевести сумму с одного счета на другой");
        System.out.println("    - при вводе в консоле команды q - Завершить выполнение программы");
    }
}
