package L4OLD.service;


import L4OLD.exception.NotEnoughMoneyException;
import L4OLD.exception.UnknownAccountException;

public interface AccountService {
   static void withdraw (int account, double amount) throws UnknownAccountException, NotEnoughMoneyException{};
   static double balance(int account) throws UnknownAccountException{return 0;};
   static void deposit(int account, double amount) throws UnknownAccountException, NotEnoughMoneyException{};
   static void transfer(int from, int to, double amount) throws UnknownAccountException, NotEnoughMoneyException{};
}
