package L4.factory;

import L4.service.AccountService;
import L4.service.FileAccountService;

public class FactoryAccountService {
    public static AccountService getAccountService(){
            return new FileAccountService();
    }
}
