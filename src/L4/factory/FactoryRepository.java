package L4.factory;

import L4.repositry.FileRepository;
import L4.repositry.Repository;

public class FactoryRepository {
    public static Repository getRepository(){
        return new FileRepository();
    }
}
