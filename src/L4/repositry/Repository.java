package L4.repositry;

import L4.domain.Account;

import java.io.IOException;

public interface Repository {
   void write(Account out) throws IOException;
   void  read() throws IOException;

}
