package app;
public class App8 {
    public static void main(String[] args) {
        int sumP = 0;
        int sumM = 0;
        double sum3 = 0;
        int kP = 0;
        int kM = 0;
        double sA = 0;

        int[] a = {1,-10,5,6,45,23,-45,-34,0,32,56,-1,2,-2};
        int max = a[0];
        for (int i = 0; i < a.length; i++) {
            if(max <= a[i]){
                max = a[i];
            }
            if (a[i] > 0) {
                sumP += a[i];
            }
            if ((a[i] < 0) && (a[i]%2 == 0)) {
                sumM += a[i];
            }
            if (a[i] > 0)  {
                kP++;
            }
            if (a[i] < 0)  {
                kM++;
                sum3 += a[i];
                sA = sum3/kM;
            }

        }
        System.out.println("- максимальное значение: " + max);
        System.out.println("- сумму положительных элементов: " + sumP);
        System.out.println("- сумму четных отрицательных элементов: " + sumM);
        System.out.println("- количество положительных элементов: " + kP);
        System.out.println("- среднее арифметическое отрицательных элементов: " + sA);
    }
}
