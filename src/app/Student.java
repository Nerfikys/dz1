package app;

public class Student {
    public String firstName;
    public String lastName;
    public String group;
    public double averageMark;
    public Student(String firstName,String lastName,String group, double averageMark){
      this.firstName=firstName;
      this.lastName=lastName;
      this.group=group;
      this.averageMark=averageMark;
    }
    public int getScholarship(double a)
    {
        if (a==5) return 100;
        else return 80;
    }
    public static void main(String[] args) {
        Student[] students= new Student[4];
        students[0]= new Student("Oleg","R","БУТ1701",4.4);
        students[1]= new Student("Korg","K","БУТ1701",3.6);
        students[2]= new Aspirant("Socrat","D","БУТ1701",3.9,"Теория Игр");
        students[3]= new Aspirant("Koma","L","БУТ1701",4.1,"Нейроные Сети");
        for (int i = 0;i<students.length;i++)
        {
            System.out.println(students[i].getScholarship(students[i].averageMark));
        }
    }
}
