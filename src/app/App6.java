package app;

import java.util.Scanner;

public class App6 {
    public static void main(String[] args) {
        System.out.println("Введите число: ");
        Scanner scanner = new Scanner(System.in);
        int value = scanner.nextInt();

        if ((value < 0) && (value%2 == 0)){
            System.out.println("отрицательное четное число");
        }
        if ((value < 0) && (value%2 != 0)){
            System.out.println("отрицательное нечетное число");
        }
        if ((value > 0) && (value%2 == 0)){
            System.out.println("положительное четное число");
        }
        if ((value > 0) && (value%2 != 0)){
            System.out.println("положительное нечетное число");
        }
        if (value == 0){
            System.out.println("нулевое число");
        }
    }
}