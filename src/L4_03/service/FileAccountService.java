package L4_03.service;

import L4_03.domain.Account;
import L4_03.exception.NotEnoughMoneyException;
import L4_03.exception.UnknownAccountException;

import L4_03.factory.FactoryRepository;
import L4_03.repositry.Repository;

import java.io.IOException;



public class FileAccountService implements AccountService {

    Repository Repository = FactoryRepository.getRepository();
     public void withdraw(int account, double amount) throws NotEnoughMoneyException, UnknownAccountException, IOException {
        boolean flag = false;
        int id = 0;
        for (int i =0;i<Account.usless.size();i++) {
                if (Account.usless.get(i).getId() == account)
                {
                    flag = true;
                    id = i;
                }
            }
               if(flag == true)
                {
                    if (Account.usless.get(id).getAmount() >= amount) {
                        Account.usless.get(id).setAmount(Account.usless.get(id).getAmount()-amount);
                        Repository.write(Account.usless.get(id));
                    } else throw new NotEnoughMoneyException();
                }
                else throw new UnknownAccountException();
    }

    public void balance(int account) throws UnknownAccountException {
        boolean flag = false;
        int id = 0;
        for (int i = 0; i < Account.usless.size(); i++) {
            if (Account.usless.get(i).getId() == account) {
                flag = true;
                id = i;
            }
        }
            if (flag) {
                System.out.println(Account.usless.get(id).getHolder());
                System.out.println(Account.usless.get(id).getAmount());
            } else throw new UnknownAccountException();

    }

    public void deposit(int account, double amount) throws UnknownAccountException, IOException {
        boolean flag = false;
        int id = 0;
        for (int i =0;i<Account.usless.size();i++) {
            if (Account.usless.get(i).getId() == account)
            {
                flag = true;
                id = i;
            }
        }
        if(flag)
        {
            Account.usless.get(id).setAmount(Account.usless.get(id).getAmount()+amount);
            Repository.write(Account.usless.get(id));
        }
    }


    public void transfer(int from, int to, double amount) throws NotEnoughMoneyException, UnknownAccountException, IOException {

        boolean flag = false;
        boolean flag2 = false;
        int id = 0;
        int id2 = 0;
        for (int i =0;i<Account.usless.size();i++) {
            if (Account.usless.get(i).getId() == from) ;
            {
                flag = true;
                id = i;
            }
        }
        for (int i =0;i<Account.usless.size();i++) {
            if (Account.usless.get(i).getId() == to)
            {
                flag2 = true;
                id2 = i;
            }
        }
        if(flag&&flag2)
        {
            if (Account.usless.get(id).getAmount() >= amount) {
                Account.usless.get(id).setAmount(Account.usless.get(id).getAmount()-amount);
                Account.usless.get(id2).setAmount(Account.usless.get(id2).getAmount()+amount);
                Repository.write(Account.usless.get(id));
                Repository.write(Account.usless.get(id2));
            } else throw new NotEnoughMoneyException();
        }
        else throw new UnknownAccountException();
    }
}
