package L4_03;

import L4_03.exception.NotEnoughMoneyException;
import L4_03.exception.UnknownAccountException;
import L4_03.factory.FactoryAccountService;
import L4_03.service.AccountService;


import java.io.IOException;

public class Menu {
    public static boolean quit;

    void starter(String what) throws UnknownAccountException, NotEnoughMoneyException, IOException {
        AccountService Service = FactoryAccountService.getAccountService();
        String[] action = what.split(" ");
        switch (action[0]) {
            case "balance":
                Service.balance(Integer.parseInt(action[1]));
                break;
            case "withdraw":
                Service.withdraw(Integer.parseInt(action[1]),Double.parseDouble(action[2]));
                Service.balance(Integer.parseInt(action[1]));
                break;
            case "deposite":
                Service.deposit(Integer.parseInt(action[1]),Double.parseDouble(action[2]));
                Service.balance(Integer.parseInt(action[1]));
                break;
            case "transfer":
                Service.transfer(Integer.parseInt(action[1]),Integer.parseInt(action[2]),Double.parseDouble(action[3]));
                Service.balance(Integer.parseInt(action[1]));
                Service.balance(Integer.parseInt(action[2]));
                break;
            case "quit":
                quit = true;
                break;
        }
        System.out.println();
    }

    void showmenu(){
        System.out.println("Меню:");
        System.out.println("    - при вводе в консоле команды balance [id] – вывеси информацию о счёте");
        System.out.println("    - при вводе в консоле команды withdraw [id] [amount] – снять указанную сумму");
        System.out.println("    - при вводе в консоле команды deposite [id] [amount] – внести на счет указанную сумму");
        System.out.println("    - при вводе в консоле команды transfer [from] [to] [amount] – перевести сумму с одного счета на другой");
        System.out.println("    - при вводе в консоле команды quit - Завершить выполнение программы");
    }
}
