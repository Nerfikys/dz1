package L4_03.factory;

import L4_03.service.AccountService;
import L4_03.service.FileAccountService;

public class FactoryAccountService {
    public static AccountService getAccountService(){
            return new FileAccountService();
    }
}
