package L4_03.factory;

import L4_03.repositry.FileRepository;
import L4_03.repositry.Repository;

public class FactoryRepository {
    public static Repository getRepository(){
        return new FileRepository();
    }
}
