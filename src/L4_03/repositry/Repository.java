package L4_03.repositry;

import L4_03.domain.Account;

import java.io.IOException;

public interface Repository {
   void write(Account out) throws IOException;
   void  read() throws IOException;

}
